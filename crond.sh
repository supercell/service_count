POST_URL=127.0.0.1/service.php
TOKEN=666

LOAD=`cat /proc/loadavg|awk '{print $1,$2,$3}'| tr ' ' ','` #负载

#ram=`cat /proc/meminfo | grep MemTotal | awk '{print $2,$3}'`

#3个内存参数为KB单位
MemAvailable=`grep 'MemAvailable' /proc/meminfo | awk '{print $2}'`
MemTotal=`grep 'MemTotal' /proc/meminfo | awk '{print $2}'`
MemUsed=$(( $MemTotal - $MemAvailable ))

#uptim时间为秒数
UPTIME=`awk '{print $1}' /proc/uptime`

function prep ()
{
	echo "$1" | sed -e 's/^ *//g' -e 's/ *$//g' | sed -n '1 p'
}
function num ()
{
	case $1 in
	    ''|*[!0-9\.]*) echo 0 ;;
	    *) echo $1 ;;
	esac
}

# Disk usage
disk_total=$(prep $(num "$(($(df -P -B 1 | grep '^/' | awk '{ print $2 }' | sed -e :a -e '$!N;s/\n/+/;ta')))"))
disk_usage=$(prep $(num "$(($(df -P -B 1 | grep '^/' | awk '{ print $3 }' | sed -e :a -e '$!N;s/\n/+/;ta')))"))

# Disk array
disk_array=$(prep "$(df -P -B 1 | grep '^/' | awk '{ print $1" "$2" "$3";" }' | sed -e :a -e '$!N;s/\n/ /;ta' | awk '{ print $0 } END { if (!NR) print "N/A" }')")


curl -s -d "key="$TOKEN"&load="$LOAD"&ram="$MemTotal"&used="$MemUsed"&free="$MemAvailable"&uptime="$UPTIME"&Disk_total="$disk_total"&Disk_usage="$disk_usage"&Disk_array="$disk_array"" $POST_URL > /dev/null
